let log = require('@dosarrest/loopback-component-logger')('perspective-<%= options.name %>/boot');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let done = me.done;
    let app = me.app;
    let ACL = app.registry.getModelByType('ACL');
    let User = app.registry.getModelByType('User');
    let <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification = app.registry.getModelByType('<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification');
    User.hasMany(<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification, {as: '<%= options.name %>Notifications', foreignKey: 'userId'});
    User.nestRemoting('<%= options.name %>Notifications');
    ACL.create({
      model: User.definition.name,
      accessType: 'READ',
      principalType: 'ROLE',
      principalId: '$owner',
      permission: 'ALLOW',
      property: '__get__<%= options.name %>Notifications'
    });
    <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification.belongsTo(User, {as: 'user', foreignKey: 'userId'});
    done();
  }
}
module.exports = Boot;
