import {LoopBackApplication, Role, PersistedModel} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('senchaloop-perspective-<%= options.name %>/server/models/<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification');

export interface <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>NotificationInterface extends PersistedModel {
  id: string;
  content: string;
  action: string;
  type: number;
  status: number;
  createdOn: Date;
}
export interface <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification extends PersistedModel {
}

class <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>NotificationModel {
  app: LoopBackApplication;
  model: <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification;
  constructor(model: <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification Model to Application');
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification: <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification) {
  new <%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>NotificationModel(<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notification);
};
