Ext.define('Client.view.perspective.<%= options.name %>.nav.menu.View', {
	extend: 'Ext.list.Tree',
	xtype: 'perspective.<%= options.name %>.nav.menu',
	ui: 'perspective-<%= options.name %>-navigation',
	requires: [
		'Ext.data.TreeStore',
	],
	scrollable: true,
	bind: {
		store: '{menu}',
		micro: '{navCollapsed}'
	},
	expanderFirst: false,
	expanderOnly: false
});
