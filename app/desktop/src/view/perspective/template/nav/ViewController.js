Ext.define('Client.view.<%= options.name %>.nav.NavViewController', {
	extend: "Ext.app.ViewController",
	alias: "controller.perspective.<%= options.name %>.navigation",
  requires: [
    'Ext.Ajax',
    'Ext.util.Cookies'
  ],

	// initViewModel: function(vm) {},

	onTopViewNavToggle: function (btn) {
		var vm = this.getViewModel();
		vm.set('navCollapsed', !vm.get('navCollapsed'));
		btn.setIconCls((vm.get('navCollapsed')?'logo-big':'x-fa fa-navicon'));
	},

	onMenuViewSelectionChange: function(tree, node) {
		if (!node) {
				return;
		}
		this.fireViewEvent("select", node);
	},

	onBottomViewlogout: function () {
	  let me = this;
    localStorage.setItem("LoggedIn", false);
    let Authentication = Client.app.getController('Authentication');
    try {
      Client.app.originalHash = window.location.hash.substring(1);
    } catch (err) {
      console.error(err);
    }
    Authentication.logout().then(() => {
      me.redirectTo('auth/login');
    }).catch(err => {
      me.redirectTo('auth/login');
    });
		// this.getView().destroy();
		// Ext.Viewport.add([{ xtype: 'loginview'}]);
	},
});
