Ext.define('Client.view.perspective.<%= options.name %>.navigation.View', {
	extend: 'Ext.Panel',
	xtype: 'perspective.<%= options.name %>.navigation',
	controller: "perspective.<%= options.name %>.navigation",
	cls: 'perspective-<%= options.name %>-navigation',
	layout: 'fit',
	tbar: {xtype: 'perspective.<%= options.name %>.nav.top', bind: { height: '{headerview_height}' }},
	items: [
		{
			xtype: 'perspective.<%= options.name %>.nav.menu',
			reference: 'menuview',
			bind: {width: '{navview_width}'},
			listeners: {
				selectionchange: "onMenuViewSelectionChange"
			}
		}
	],
	bbar: {xtype: 'perspective.<%= options.name %>.nav.bottom', bind: {height: '{bottomview_height}'}}
});
