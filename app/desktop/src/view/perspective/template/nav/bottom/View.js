Ext.define('Client.view.perspective.<%= options.name %>.nav.bottom.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.<%= options.name %>.nav.bottom',
	cls: 'perspective-<%= options.name %>-nav-bottom',
	shadow: false,
	items: [
		{
			xtype: 'button',
			ui: 'perspective-<%= options.name %>-nav-bottombutton',
      margin: '0 0 0 6px',
			iconCls: 'x-fa fa-angle-double-left',
      bind: {
        text: '{"per-<%= options.name %>:LOGOUT":translate}',
        tooltip: '{"per-<%= options.name %>:LOGOUT":translate}',
      },
			handler: 'onBottomViewlogout'
		}
	]
});
