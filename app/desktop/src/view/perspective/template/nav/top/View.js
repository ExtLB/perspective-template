Ext.define('Client.view.perspective.<%= options.name %>.nav.top.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.<%= options.name %>.nav.top',
	cls: 'perspective-<%= options.name %>-nav-top',
	shadow: false,
	items: [
		{
			xtype: 'container',
			cls: 'perspective-<%= options.name %>-nav-toptext',
			bind: {
				html: '{"per-<%= options.name %>:TITLE":translate}',
				hidden: '{navCollapsed}'
			}
		}, '->', {
			xtype: 'button',
			ui: 'perspective-<%= options.name %>-nav-topbutton',
      bind: {
			  tooltip: '{"per-<%= options.name %>:TOP_MENU_BTN_TOOLTIP":translate}'
      },
			reference: 'navtoggle',
			handler: 'onTopViewNavToggle',
			iconCls: 'x-fa fa-navicon'
		}
	]
});
