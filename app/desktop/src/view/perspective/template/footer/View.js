Ext.define('Client.view.perspective.<%= options.name %>.footer.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.<%= options.name %>.footer',
	cls: 'perspective-<%= options.name %>-footer',

	items: [
		{
			xtype: 'container',
			cls: 'perspective-<%= options.name %>-footertext',
      bind: {
        html: '{"per-<%= options.name %>:FOOTER":translate}'
      }
		},
//		'->',
//		{
//			xtype: 'button',
//			ui: 'footerbutton',
//			iconCls: 'x-fa fa-automobile'
//		}
	]
});
