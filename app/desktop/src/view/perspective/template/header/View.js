Ext.define('Client.view.perspective.<%= options.name %>.header.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.<%= options.name %>.header',
	cls: 'perspective-<%= options.name %>-header',
  requires: [
    'Ext.XTemplate',
    'Ext.dataview.plugin.ListPaging',
    'Ext.dataview.List',
    'Ext.menu.Menu',
    'Ext.Img'
  ],
	items: [
		{
			xtype: 'container',
			cls: 'perspective-<%= options.name %>-headertext',
			bind: { html: '{heading:<%= options.name %>Header}' }
		}, '->', {
	    xtype: 'container',
      reference: 'headerBtns',
      padding: 5,
      items: [{
	      xtype: 'button',
        ui: 'round raised',
        bind: {
	        tooltip: '{"per-<%= options.name %>:ADMINISTRATION":translate}',
          hidden: '{!isAdmin}'
        },
        reference: 'adminBtn',
        handler: 'onAdminBtn',
        iconCls: 'x-fa fa-cogs',
        margin: '0 5px 0 0'
      }, {
        xtype: 'button',
        ui: 'round raised',
        bind: {
          tooltip: '{"per-<%= options.name %>:NOTIFICATIONS":translate}'
        },
        reference: 'notificationsBtn',
        handler: 'onNotificationsBtn',
        iconCls: 'x-fa fa-bell',
        margin: '0 5px 0 0'
      }, {
        xtype: 'button',
        ui: 'rounded raised normalcase',
        bind: {
          text: '{user.username}',
          tooltip: '{"per-<%= options.name %>:PROFILE":translate}'
        },
        reference: 'profileBtn',
        handler: 'onProfileBtn',
        margin: '0 5px 0 0'
      }],
    }, {
      xtype: 'image',
      cls: 'circular raised',
      height: 35,
      width: 35,
      alt: 'My Profile Image',
      src: 'resources/shared/images/company-logo.png'
    }, {
	    xtype: 'menu',
      reference: 'notificationsMenu',
      itemId: 'notificationsMenu',
      maxWidth: 500,
      minWidth: 350,
      anchor: true,
      items: [{
	      xtype: 'list',
        reference: 'notificationsList',
        itemId: 'notificationsList',
        margin: 0,
        maxHeight: 500,
        maxWidth: 500,
        minHeight: 30,
        minWidth: 350,
        scrollable: 'vertical',
        itemTpl: [
          '<div>{createdOn:date("Y/m/d H:i:s")}</div>',
          '<div>{content}</div>'
        ],
        bind: {
          store: '{notifications}',
        },
        selectable: {
	        mode: 'simple',
        },
        itemConfig: {
	        xtype: 'simplelistitem',
        },
        plugins: [{
	        type: 'listpaging',
        }]
      }]
    }
	]
});
