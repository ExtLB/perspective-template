Ext.define('Client.view.<%= options.name %>.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.perspective.<%= options.name %>',

	routes: {
		'<%= options.name %>(/:{routeId})(/:{arg0})(/:{arg1})(/:{arg2})': {action: '<%= options.name %>Route'}
	},
	<%= options.name %>Route:function(params) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    let Authentication = Client.app.getController('Authentication');
    if (Authentication.loggedIn === true) {
      let user = vm.get('user');
      try {
        if (user.id !== Authentication.user.id) {
          vm.set('user', Authentication.user);
        }
      } catch (err) {
        console.error(err);
      }
      let routeId = params.routeId;
      try {
        // console.log('<%= options.name %> Route Executing');
        //var menuview = this.lookup('menuview');
        let navview = me.lookup('navview');
        let menuview = navview.items.items[0];

        let centerview = me.lookup('centerview');
        if (_.isUndefined(routeId)) {
          console.log('Route Id is undefined');
        } else {
          console.log('Route Id is defined as', routeId);
        }

        let menuStore = menuview.getStore();
        function navigate() {
          let exists = Ext.ClassManager.getByAlias('widget.' + routeId);
          if (exists === undefined) {
            console.log(routeId + ' does not exist');
            return;
          }
          let node = menuStore.findNode('routeId', routeId);
          if (node == null) {
            console.log('unmatchedRoute: ' + routeId);
            return;
          }
          if (!centerview.getComponent(routeId)) {
            centerview.add({xtype: routeId, itemId: routeId, heading: node.get('text')});
          }
          centerview.setActiveItem(routeId);
          menuview.setSelection(node);
          let vm = me.getViewModel();
          vm.set('heading', [node.get('text')]);
        }
        if (menuStore.isLoaded() === false || menuStore.isLoading() === true || menuStore.root.hasChildNodes() === false) {
          menuStore.on('load', () => {
            if (_.isUndefined(routeId) && menuStore.count() > 0) {
              routeId = menuStore.first().get('routeId');
              me.redirectTo(`<%= options.name %>/${routeId}`);
            } else {
              navigate();
            }
          }, me, {
            single: true
          });
          if (menuStore.isLoading() === false && menuStore.root.get('loading') === false) {
            menuStore.load();
          }
        } else {
          if (_.isUndefined(routeId) && menuStore.count() > 0) {
            routeId = menuStore.first().get('routeId');
            me.redirectTo(`<%= options.name %>/${routeId}`);
          } else {
            navigate();
          }
        }
      } catch (err) {
        console.error(err);
      }
    } else {
      Client.app.originalHash = window.location.hash.substring(1);
      me.redirectTo('auth/login');
    }
	},

	onMenuViewSelectionChange: function (tree, node) {
		if (node == null) { return }
		var vm = this.getViewModel();
		if (node.get('routeId') !== undefined) {
			this.redirectTo( `<%= options.name %>/${node.get('routeId')}` );
		}
	},

	onTopViewNavToggle: function () {
		var vm = this.getViewModel();
		vm.set('navCollapsed', !vm.get('navCollapsed'));
	},

  onAdminBtn: (button) => {
	  let Navigation = Client.app.getController('Navigation');
	  Navigation.gotoPerspective('admin');
  },

  onNotificationsBtn: (button) => {
	  let view = Ext.Viewport.down('perspective\\.<%= options.name %>');
    // let vm = view.getViewModel();
    let vc = view.getController();
    let menu = vc.lookup('notificationsMenu');
    menu.showBy(button, 't-b?');
  },

  on<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>ViewInitialize: function () {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    let Authentication = Client.app.getController('Authentication');
    if (Authentication.loggedIn === true) {
      vm.set('user', Authentication.user);
    }
    Authentication.on('login', () => {
      vm.getStore('notifications').load();
      let user = Authentication.user;
      vm.set('user', user);
      let adminRole = _.find(user.roles, (role) => { return role.name === 'Administrator'; });
      vm.set('isAdmin', !_.isUndefined(adminRole));
    });
    Ext.apply(Ext.util.Format, {
      <%= options.name %>Header: function (value) {
        return _.join(value.map((v) => { return i18next.t(v); }), ' > ');
      },
    });
    // setTimeout(() => {
    //   vm.getStore('desktopmenu').load();
    // }, 1);
  }

//	onActionsViewLogoutTap: function( ) {
//		var vm = this.getViewModel();
//		vm.set('firstname', '');
//		vm.set('lastname', '');
//
//		Session.logout(this.getView());
//		this.redirectTo(AppCamp.getApplication().getDefaultToken().toString(), true);
//	}

});
