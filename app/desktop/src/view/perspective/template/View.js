Ext.define('Client.view.perspective.<%= options.name %>.View', {
	extend: 'Ext.Container',
	xtype: 'perspective.<%= options.name %>',
	controller: {type: 'perspective.<%= options.name %>'},
	viewModel: {type: 'perspective.<%= options.name %>'},
  requires: ['Ext.layout.Fit'],
	layout: 'fit',
  listeners: {
	  initialize: 'on<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>ViewInitialize'
  },
	items: [
		{ xtype: 'perspective.<%= options.name %>.navigation',    reference: 'navview',    docked: 'left',   bind: {width:  '{navview_width}'}, listeners: { select: "onMenuViewSelectionChange"} },
		{ xtype: 'perspective.<%= options.name %>.header', reference: 'headerview', docked: 'top',    bind: {height: '{headerview_height}', hidden: '{!showHeader}'} },
		{ xtype: 'perspective.<%= options.name %>.footer', reference: 'footerview', docked: 'bottom', bind: {height: '{footerview_height}', hidden: '{!showFooter}'} },
		{ xtype: 'perspective.<%= options.name %>.center', reference: 'centerview' },
	]
});
