Ext.define('Client.view.perspective.<%= options.name %>.center.View', {
	extend: 'Ext.Container',
	xtype: 'perspective.<%= options.name %>.center',
	cls: 'perspective-<%= options.name %>-center',
	layout: 'card'
});
