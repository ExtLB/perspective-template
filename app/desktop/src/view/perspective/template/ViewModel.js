Ext.define('Client.view.<%= options.name %>.ViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.perspective.<%= options.name %>',
  requires: [
    'Client.model.perspective.<%= options.name %>.NavigationItem'
  ],
	data: {
		name: 'Client',
    apiUrl: '/api/v1',
    showHeader: true,
    showFooter: true,
		navCollapsed:       false,
    notificationsCollapsed: false,
		navview_max_width:    300,
		navview_min_width:     60,
		topview_height:        75,
		bottomview_height:     50,
		headerview_height:     50,
		footerview_height:     50,
    user: {
		  username: 'Username',
    },
    menuExtraParams: {
      filter: '{"order": "weight ASC"}'
    },
	},
	formulas: {
		navview_width: function(get) {
			return get('navCollapsed') ? get('navview_min_width') : get('navview_max_width');
		}
	},
	stores: {
		menu: {
		  autoLoad: false,
			type: 'tree',
      model: 'Client.model.perspective.<%= options.name %>.NavigationItem',
      proxy: {
			  type: 'rest',
        url: '{apiUrl}',
        extraParams: '{menuExtraParams}',
        reader: {
			    type: 'json'
        }
      },
			root: {
			  id: '<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Navigation',
				expanded: true,
				children: []
			}
		},
    notifications: {
		  autoLoad: false,
      model: 'Client.model.perspective.<%= options.name %>.Notification',
      proxy: {
			  type: 'rest',
        url: '{apiUrl}/Users/me/<%= options.name.charAt(0).toUpperCase() + options.name.substr(1) %>Notifications',
        reader: {
			    type: 'json',
          rootProperty: 'data'
        }
      }
		}
	}
});
